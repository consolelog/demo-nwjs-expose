# Nw.js应用支持浏览器访问

原理就是使用 [http-proxy-middleware](https://www.npmjs.com/package/http-proxy-middleware) 中间件以类似nginx的方式开放端口号提供给浏览器访问。

通过这种代理的方式暴露 [nw.js](https://nwjs.io/) API的功能，无法在浏览器正常使用，而是会远程调用到nw客户端生效，所以可以实现一些奇怪的功能。

举个例子，比如本机开启nw客户端并暴露了3000端口，开放了一个api实现最小化nw客户端窗口的功能；
此时局域网a用户通过浏览器访问ip:3000/api这个链接，在成功访问后，a用户的浏览器没有任何效果，而本机的nw客户端则会最小化。

不用这种代理的方式，而是正常实现的nwjs功能不受影响，仍然可以在nw客户端正常使用。
