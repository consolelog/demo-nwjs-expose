const express = require('express');
const {createProxyMiddleware} = require('http-proxy-middleware');
const fs = require('node:fs');
const app = express();

app.get('/napi*', (req, res, next) => {
  // url:/napi/existsSync/
  let url = req.path;
  if (url.startsWith('/napi/existsSync')) {
    existsSync(req, res)
    return
  }
  next()
});

function existsSync(req, res) {
  // path: D:/testFolder
  const path = req.query.path;
  if (!path) {
    res.status(400).send('Path parameter is missing');
    return;
  }
  // 执行 fs.existsSync(path) 功能
  const exists = fs.existsSync(path);
  // 返回结果
  res.json(exists);
}

app.use(express.static('.'))

app.listen(36900, '127.0.0.1');
