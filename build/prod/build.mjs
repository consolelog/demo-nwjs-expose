import fs from 'node:fs'
import path from "node:path";
import * as child_process from "node:child_process";
import {getProcessArg, nwBuilder} from "wang-nw-builder";

child_process.execSync('npm run build', {stdio: 'inherit'})
if (fs.existsSync('./temp')) fs.rmdirSync('./temp', {recursive: true})
fs.cpSync('./dist/demo-nwjs-expose/browser', './temp', {recursive: true})
fs.copyFileSync('./build/prod/bgscript.js', './temp/bgscript.js')
fs.copyFileSync('./build/prod/main.js', './temp/main.js')
fs.copyFileSync('./build/prod/node-main.js', './temp/node-main.js')
fs.copyFileSync('./build/prod/package.json', './temp/package.json')
let dept = ['express', 'http-proxy-middleware']
copyDependencies(dept)

nwBuilder({
  srcDir: './temp',
  platform: getProcessArg('platform') ?? 'win',
  outDir: `./out/${getProcessArg('platform') ?? 'win'}`,
  logLevel: 'debug',
  version: '0.82.0',
  flavor: 'sdk'
}).then(() => fs.rmSync('./temp', {recursive: true}))


function copyDependencies(deptList) {
  const npmLockStr = fs.readFileSync(path.join('.', 'package-lock.json'), 'utf-8')
  let parse = JSON.parse(npmLockStr);
  let list = deptList.map(s => {
    return resolveDeptTree(parse, s)
  });
  list = [].concat(...list)
  if (list && list.length > 0) fs.mkdirSync('./temp/node_modules', {recursive: true})
  for (let dept of list) {
    let sourcePath = `./node_modules/${dept}`;
    if (!fs.existsSync(sourcePath)) continue
    let targetPath = `./temp/node_modules/${dept}`;
    if (fs.existsSync(targetPath)) continue
    fs.cpSync(sourcePath, targetPath, {recursive: true})
  }
}

function resolveDeptTree(npmLock, deptName) {
  let findResult = npmLock.packages[`node_modules/${deptName}`];
  let r = []
  if (!findResult) return r;
  r.push(deptName)
  if (findResult.dependencies) {
    let children = Object.entries(findResult.dependencies).map(k => {
      return resolveDeptTree(npmLock, k[0])
    })
    r = r.concat(...children)
  }
  return r
}
