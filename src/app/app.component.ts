import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { NwjsService } from './nwjs.service';
import { FormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, FormsModule],
  template: `
      <h1>判断本地目录是否存在</h1>
      路径：<input type="text" [(ngModel)]="path">
      <button (click)="check()">判断</button>
      <br>
      结果：{{ message }}
      <br>
      <button (click)="minimize()">最小化窗口</button>
      <router-outlet></router-outlet>
  `,
  styles: [],
})
export class AppComponent {
  message: any = null;
  path = '/';

  constructor(private nwjs: NwjsService, private http: HttpClient) {
  }

  check() {
    this.nwjs.existsSync(this.path).subscribe(r => {
      console.log('r', r);
      this.message = r;
    });
  }

  minimize() {
    this.http.get(`/napi/minimize`).subscribe();
  }
}
